clear; close all; clc;
format long g;

% Approximate solving of non-linear equations 


% ------------------------- Bisection Method ------------------------------
int_beg = -1;
int_end = 2;
acc = 0.001;

x1_B = 1/2*(int_beg+int_end);
xs_B = 0;

iteration = 0;
while (~(abs(fB(x1_B)) <= acc))
    iteration = iteration + 1;
    
    disp(fB(x1_B));
    if (fB(int_beg)*fB(x1_B) < 0)
        int_end = x1_B;
        x1_B = 1/2*(int_beg+int_end);
    end
    if (fB(int_end)*fB(x1_B) < 0)
        int_beg = x1_B;
        x1_B = 1/2*(int_beg+int_end);
    end
end

% ---------------------------- Result -------------------------------------
disp(xs_B);
disp('x1');
disp(x1_B);
disp('f(x1)');
disp(fB(x1_B));
disp('iterations');
disp(iteration);

% ---------------------- Regula falsi method ------------------------------

x1_RF = int_beg - fB(int_beg)/(fB(int_beg)