clear; close all; clc;
format long g;

% Approximate solving of non-linear equations 


% ---------------------- Regula Falsi Method ------------------------------
int_beg = -10;
int_end = -1;
acc = 0.001;

x1 = int_beg - f(int_beg)/(f(int_end)-f(int_beg))*(int_end-int_beg);
disp('x1');
disp(x1);
disp('f(x1)');
disp(f(x1));
iteration = 1;

while (abs(f(x1))>acc)
    iteration = iteration + 1;
    
    if ((f(int_beg)<0 && f2(int_beg)<0) || (f(int_beg)>0 && f2(int_beg)>0))
        x1 = x1 - (f(x1)/(f(x1)-f(int_beg)))*(x1-int_beg);
    end
    
    if ((f(int_end)<0 && f2(int_end)<0) || (f(int_end)>0 && f2(int_end)>0))
        x1 = x1 - (f(x1)/(f(int_end)-f(x1)))*(int_end-x1);
    end
    
    disp("x"+iteration);
    disp(x1);
    disp("f(x"+iteration+")");
    disp(f(x1));
end

% -------------------------- Newton Method --------------------------------

if ((f(int_beg)<0 && f2(int_beg)<0) || (f(int_beg)>0 && f2(int_beg)>0))
    x1 = int_beg;
end
    
if ((f(int_end)<0 && f2(int_end)<0) || (f(int_end)>0 && f2(int_end)>0))
    x1 = int_end;
end

iteration = 0;
while (abs(f(x1))>acc)
    iteration = iteration + 1;
    
    x1 = x1 - f(x1)/f1(x1);
    
    disp("x"+iteration);
    disp(x1);
    disp("f(x"+iteration+")");
    disp(f(x1));
end
