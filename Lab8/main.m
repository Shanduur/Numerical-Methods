clear; close all; clc;

syms p1 p2 p3;

% A = [1 2.5 3; 1 4 3; 1 2 6];
 A = [1 1 1; 1 2 3; 1 3 6];
y = zeros(1,length(A));
y(1,1) = 1;

y = transpose(y);

z = zeros(length(A),length(A));

% ------------------ Eigenvalues ------------------

for i=1:length(A)
    for j=1:length(A)
        z(i,j) = y(j);
    end
    y = A*y;
end

for i=1:length(A)
    disp("Y(" + (i-1) +")");
    for j=1:length(A)
        disp(z(i,j));
    end
end

disp("Y(" + (length(A)-1) +")");
for i=1:length(A)
    disp(y(i));
end

eq1 = z(3,1)*p1 + z(2,1)*p2 + z(1,1)*p3 == -y(1);
eq2 = z(3,2)*p1 + z(2,2)*p2 + z(1,2)*p3 == -y(2);
eq3 = z(3,3)*p1 + z(2,3)*p2 + z(1,3)*p3 == -y(3);

[sol1,sol2] = equationsToMatrix([eq1, eq2, eq3], [p1, p2, p3]);
 
eig = linsolve(sol1,sol2);

disp("Eigenvalues:");
disp(eig);


% ------------------ Eigenvectors ------------------
poly = zeros(1,length(A)+1);

for i=1:length(A)
    poly(i) = -eig(length(A)+1-i);
end
poly(length(A)+1) = -1;

disp(poly);
rts = roots(poly);
disp(rts);

g3 = 1;
g2 = double(rts(3) + eig(3));
g1 = double(g2*rts(3) + eig(2));

x = zeros(length(A), length(A));

x1=g1*z(1,:) + g2*z(2,:);

disp(x1);