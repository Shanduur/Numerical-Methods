function [out] = f(x,y)
    a = -0.3; b = 1.0; c = -1.0;
    out = a*y + b*exp(c*x);
end