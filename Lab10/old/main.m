clear all
close all
clc

a = -0.3; b = 1.0; c = -1.0; d = 5.0; e = 20;
% a = 3.0; b = 6.0; c = 0; d = 0; e = 5;

h = 1;
x0 = 0;
y0 = d;
total_delta = 0;
iterations = 0;

while (x0 < e)
    delta_sum = 0.0;
    x_tmp = x0;
    y_tmp = y0;
    
    k = h*f(x0,y0);
    delta_sum = delta_sum + k;
        
    x0 = x_tmp + h/2;
    y0 = y_tmp + k/2;
    k = h*f(x0,y0);
    delta_sum = delta_sum + 2*k;
        
    x = x0 + h/2;
    y = y0 + k/2;
    k = h*f(x0,y0);
    delta_sum = delta_sum  + 2*k;
        
    x = x0 + h;
    y = y0 + k;
    k = h*f(x0,y0);
    disp("" + x + "     " + y);
    delta_sum = delta_sum + k;
    total_delta = total_delta + delta_sum/6;   
    iterations = iterations + 1;
end