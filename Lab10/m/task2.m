clear all;
close all;
clc;

a=-0.3;
b=1;
c=-1;
d=5;

%range - end step
e=20;
%step
h = 1;

step = e/h;

syms x y
f(x,y)=a*y+b*exp(c*x);
X=zeros(1,step);
Y=zeros(1,step);
delY=zeros(1,step);
k=zeros(1,4);

Y(1)=d;

for i = 1:step
    k(1)= h*f(X(i),Y(i));
    k(2)= h*f(X(i)+(1/2)*h,Y(i)+(1/2)*k(1));
    k(3)= h*f(X(i)+(1/2)*h,Y(i)+(1/2)*k(2));
    k(4)= h*f(X(i)+h,Y(i)+k(3));
    
    delY(i)=(1/6)*(k(1)+2*k(2)+2*k(3)+k(4));
    if i == step
    else
        Y(i+1)=Y(i)+delY(i);
        X(i+1)=X(1)+i*h;
    end
end
disp(Y);

p = figure;
hold on;
plot(X,Y);
xlabel("X");
ylabel("Y");
title("Task 2 - Implement 4th order Runge-Kutta method");
saveas(p, 'task2.png');

Y2=zeros(1,length(X));
for i=1:length(X)
    Y2(i)=(-1.42857*exp(-0.7*X(i))+6.42857)/exp(0.3*X(i));
end
disp(Y2)
error = Y2-Y