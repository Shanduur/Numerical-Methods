clear all;
close all;
clc;

a=-0.3;
b=1;
c=-1;
d=5;

%range - end step
e=20;
%step
h = 1;

step = e/h;

syms x y
f(x,y)=a*y+b*exp(c*x);
X=zeros(1,step);
Y=zeros(1,step);
delY=zeros(1,step);
Y(1)=d;

for i = 1:step
    delY(i)=h*f(X(i),Y(i));
    if i == step
    else
        Y(i+1)=Y(i)+delY(i);
        X(i+1)=X(i)+h;
    end
end
disp(Y);

p = figure;
hold on;
plot(X,Y);
xlabel("X");
ylabel("Y");
title("Task 1 - Implement Euler method");
saveas(p, 'task1.png');

Y2=zeros(1,length(X));
for i=1:length(X)
    Y2(i)=(-1.42857*exp(-0.7*X(i))+6.42857)/exp(0.3*X(i));
end

error = Y2-Y
