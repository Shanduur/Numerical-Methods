#include <iostream> 
#include <math.h>

#define __VERSION__ 2
#define __H__ 0.5

struct _dat
{
	float x = 0; /// #0# <= x <= e
	float a; /// y'= #A# y  +  B  e^  C  x
	float b; /// y'=  A  y  + #B# e^  C  x
	float c; /// y'=  A  y  +  B  e^ #C# x
	float d; /// y(0) = #D#
	float e; /// 0 <=x <= #e#
} d;

float func(float x, float y)
{
	return (d.a * y) + d.b * (exp(d.c * x));
}

void euler(float xi, float y, float h, float x)
{
	std::cout << "x\t|\ty\t|\tf(x,y)\t|\tdelta(y)" << std::endl;
	std::cout << "_______________________________________________________________________" << std::endl;
	while (xi < x)
	{
		std::cout << xi << "\t|\t" << y << "\t|\t" << func(xi, y) << "\t|\t" << h * func(xi, y) << std::endl;
		y = y + h * func(xi, y);
		xi = xi + h;
	}
	std::cout << xi << "\t|\t" << y << "\t|\t" << func(xi, y) << "\t|\t" << h * func(xi, y) << std::endl;
}

void ver(int v)
{
	switch (v)
	{
	case 1:
		d.a = -0.3;
		d.b = 1.0;
		d.c = -1.0;
		d.d = 5.0;
		d.e = 20.0;
		break;

	case 2:
		d.a = 3.0;
		d.b = 6.0;
		d.c = 0.0;
		d.d = 0.0;
		d.e = 5.0;
		break;

	default:
		d.a = 0;
		d.b = 0;
		d.c = 0;
		d.d = 0;
		d.e = 0;
		break;
	}
}

int main()
{
	ver(__VERSION__);
	euler(d.x, d.d, __H__, d.e);
	return 0;
}
