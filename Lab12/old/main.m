clear all; close all; clc;

a = 0;
b = 3;

x = a:0.1:b;
y_n = ones(length(x),100);

for i=1:length(x)
    for n=1:100
        y_n(n) = yn(x, y_n, n);
    end
end

fig = figure();
hold on;
plot(x,y_n,'-r');
xlabel('x');
ylabel('y');
title('Plot 1');
saveas(fig, 'plot_1.png'); 

function [out] = yn(x, y_n, n)
    iteration = 0;
    for i=1:length(x)
        
        suma = 0;
        
        for j=1:i
            
            if ((n == 1 && j==1) || j == 1)
                suma = suma + K(x(i)-x(j)) * 1;
            end
            
            if (n ~= 1 && j ~= 1)
                suma = suma + K(x(i)-x(j)) * y_n(i, j-1); % TODO;
            end
            iteration = iteration + 1
        end
        
        y(i) = -1*suma+1;
    end
    y
    out = y;

    function [out] = K(t)
        out = exp(-t);
    end
end

