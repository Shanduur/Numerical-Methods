clear all;
close all;
clc;

a = 0;
b = 3;
lambda = -1;
iterations = 100;

x = a:0.01:b;
y = ones(1, length(x));
y_n = ones(length(x),iterations);

for i=1:length(x)
    for n=1:iterations
        for j=1:i
            suma = 0;
            if (n-1 == 0)
                suma = suma + K(x(i)-x(j)) * 1;
            else
                if (i-1 == 0)
                    suma = suma + K(x(i)-x(j)) * 1;
                else
                    suma = suma + K(x(i)-x(j)) * y_n(i-1, n-1);
                end
            end
        end
        y_n(i, n) = lambda*suma+f();
    end 
    y(i) = y_n(i,100); 
end

fig = figure();
hold on;
plot(x,y,'-r');
xlabel('x');
ylabel('y');
title('Plot 1');
saveas(fig, 'plot_1.png'); 

function [out] = f(~)
    out = 1;
end

function [out] = K(x)
    out = exp(-x);
end