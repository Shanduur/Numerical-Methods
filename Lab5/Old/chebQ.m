function [ out ] = chebQ( n )
out  = zeros(1,n);
if ( n == 2 )
    out(1) = -0.577350;
    out(2) = 0.577350;
end
if ( n == 3 )
    out(1) = -0.707107;
    out(2) = 0;
    out(3) = 0.707107;
end
if ( n == 4 )
    out(1) = -0.794654;
    out(2) = -0.187592;
    out(3) = 0.187592;
    out(4) = 0.794654;
end
if ( n == 5 )
    out(1) = -0.832498;
    out(2) = -0.374541;
    out(3) = 0;
    out(4) = 0.374541;
    out(5) = 0.832498;
end
if ( n == 6 )
    out(1) = -0.866247;
    out(2) = -0.422519;
    out(3) = -0.266635;
    out(4) = 0.266635;
    out(5) = 0.422519;
    out(6) = 0.866247;
end
if ( n == 7 )
    out(1) = -0.883862;
    out(2) = -0.529657;
    out(3) = -0.323912;
    out(4) = 0;
    out(5) = 0.323912;
    out(6) = 0.529657;
    out(7) = 0.883862;
end
end

