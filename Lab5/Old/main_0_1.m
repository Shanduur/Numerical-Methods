close all; clc; clear;

% Data
interval_start = 1;
interval_end = 4;
step = 0.1;
% function [ out ] = f(x)
% out = x^2-x+1;
% end;

% Plot
points = 999;
step_plot = (interval_end-interval_start)/points;
x_plot = interval_start:step_plot:interval_end;
y_plot = zeros(1,length(x_plot));
for i=1:length(x_plot)
    y_plot(i) = f(x_plot(i));
end
%plot(x_plot,y_plot,'-r');

% Analytical formula
outAnal = ff(interval_end)-ff(interval_start);
disp("Analytical formula");
disp(outAnal);

% ------------------- Newton-Cotes method -------------------
% Squared formula
midS = (interval_end - interval_start)/2;
outSqua = midS*f(midS);
errSqua = outSqua - outAnal;
disp("Squared formula");
disp(outSqua);
disp("Error");
disp(errSqua);


% Trapezoid formula
hTrap = interval_end - interval_start; 

outTrap = (1/2) * hTrap * ( f(interval_start) + f(interval_end) );
errTrap = (-1/12) * hTrap^3 * f2(hTrap);

disp("Trapezoid formula");
disp(outTrap);
disp("Error:");
disp(errTrap);

% Simpson formula
x0s = interval_start;
x1s = ( interval_start + interval_end ) / 2;
x2s = interval_end;
hSimp = hTrap/2;

outSimp = (1/3) * hSimp * ( f(x0s) + 4*f(x1s) + f(x2s));
errSimp = (1/90) * hSimp^5 * f4(x2s-x0s);

disp("Simpson formula");
disp(outSimp);
disp("Error:");
disp(errSimp);

% ------------------- Chebyshev formula -------------------
for n=2:2:4
    xc = zeros(1,n); 
    cheb = chebQ(n);
    for i=1:n
        xc(i)= (1/2)*(interval_end+interval_start)+ (1/2)*(interval_end-interval_start)*cheb(i);
    end

    outCheb = 0;
    for i=1:n
        outCheb = outCheb + f(xc(i));
    end
    outCheb = outCheb*(interval_end-interval_start)/2 ;

    disp("Chebyshev formula for n = " + n);
    disp(outCheb);
end


% ------------------- Composite quadrature Formulae -------------------
% Trapezoid formula

m = 20;
x = interval_start:(interval_end-interval_start)/m:interval_end;
sumTrap = 0;
hTrapQ = ( interval_end - interval_start ) / m;

for i=1:m-1
    sumTrap = sumTrap + (( f(x(i)) + f(x(i+1)) ) / 2);
end
outTrapQ = sumTrap * hTrapQ;

x_q = zeros(1,m);
for i=1:m
    x_q(i) = f2(x(i));
end

errTrapQ = ( m * hTrapQ^3 ) / 12 * max(abs(x_q));
disp("Trapezoid composite quadrature formula:");
disp(outTrapQ);
disp("Trapezoid composite quadrature error:");
disp(errTrapQ); % this looks not as it should, idk why

% Simpson formula
stepSimp = ( interval_end - interval_start ) / m;
xSimp = interval_start:stepSimp:interval_end;
mSimp = length(xSimp);
hSimpQ = ( interval_end - interval_start ) / mSimp;
sumSimp = 0;
sumSimp = sumSimp + f(xSimp(1)) + f(xSimp(mSimp));

tmp1 = 0;
for i=1:mSimp/2-1
    tmp1 = tmp1 + 4*f(xSimp(2*i));
end

tmp2 = 0;
for i=1:mSimp/2-1
    tmp2 = tmp2 + 2*f(xSimp((2*i)-1));
end
sumSimp = sumSimp + tmp1 + tmp2;
outSimpQ = (hSimpQ/3)*sumSimp;

x_qs = zeros(1,mSimp);
for i=1:mSimp
    x_qs(i) = f4(xSimp(i));
end
errSimpQ = (-1/90)*mSimp*hSimpQ^5*max(abs(x_qs));

disp("Simpson composite quadrature formula:");
disp(outSimpQ);
disp("Simpson composite quadrature error:");
disp(errSimpQ); % this looks not as it should
