close all; clc; clear;

% Data
interval_start = -1;
interval_end = 1;


% Plot
points = 999;
step = (interval_end-interval_start)/points;
x_plot = interval_start:step:interval_end;
y_plot = zeros(1,length(x_plot));
for i=1:length(x_plot)
    y_plot(i) = f(x_plot(i));
end
%plot(x_plot,y_plot,'-r');

% Analytical formula
outAnal = ff(interval_end)-ff(interval_start);
disp("Analytical formula");
disp(outAnal);

% Squared formula
midS = (interval_end - interval_start)/2;
outSqua = midS*f(midS);
errSqua = outSqua - outAnal;
disp("Squared formula");
disp(outSqua);
disp("Error");
disp(errSqua);

% Trapezoid formula
hTrap = interval_end - interval_start; 

outTrap = (1/2) * hTrap * ( f(interval_start) + f(interval_end) );
errTrap = (-1/12) * hTrap^3 * f2(hTrap);

disp("Trapezoid formula");
disp(outTrap);
disp("Error:");
disp(errTrap);

% Simpson formula
x0s = interval_start;
x1s = ( interval_start + interval_end ) / 2;
x2s = interval_end;
hSimp = hTrap/2;

outSimp = (1/3) * hSimp * ( f(x0s) + 4*f(x1s) + f(x2s));
errSimp = (1/90) * hSimp^5 * f4(x2s-x0s);

disp("Simpson formula");
disp(outSimp);
disp("Error:");
disp(errSimp);
