close all; clc; clear;

properValue = 17.703;
a=-2;
b=2;

%----------------------------------SIMPLE----------------------------------

% Rectangle
y_Rect=(b-a)*f(0);

disp('Rectangle');
disp(y_Rect);
disp('Error');
disp(-y_Rect+properValue);

% Trapezoid
h=b-a;

y_Trap = h/2*(f(a)+f(b));

disp('Trapezoid');
disp(y_Trap);
disp('Error');
disp(-y_Trap+properValue);

% Simpson
h =(b-a)/2;

y_Simp=(1/3)*(f(a)+4*f(a+h)+f(b));

disp('Simpson');
disp(y_Simp);
disp('Error');
disp(-y_Simp+properValue);

% Newton-Cotes for n=4
h = (b-a)/4;

y_NewtCot_4 = (2/45)*h*(7*f(a)+32*f(a+h)+12*f(a+2*h)+32*f(a+3*h)+7*f(b));

disp('Newton-Cotes n=4');
disp(y_NewtCot_4);
disp('Error');
disp(-y_NewtCot_4+properValue);

% ChebyshewRectangle for n=2
n=2;

x1 = 0.5*(a+b)+0.5*(a-b)*(-0.577350);
x2 = 0.5*(a+b)+0.5*(a-b)*(0.577350);

y_ChebRect_2 = ((b-a)/n)*(f(x1)+f(x2));

disp('Chebyshew n=2');
disp(y_ChebRect_2);
disp('Error');
disp(-y_ChebRect_2+properValue);

% ChebyshewRectangle for n=4
x1 = 0.5*(a+b)+0.5*(a-b)*(-0.794654);
x2 = 0.5*(a+b)+0.5*(a-b)*(-0.187592);
x3 = 0.5*(a+b)+0.5*(a-b)*(0.187592);
x4 = 0.5*(a+b)+0.5*(a-b)*(0.794654);

yChebyshew4 = ((b-a)/n)*(f(x1)+f(x2)+f(x3)+f(x4));

disp('Chebyshew n=4');
disp(yChebyshew4);
disp('Error');
disp(-yChebyshew4+properValue);

%----------------------------------COMPOSITE----------------------------------

% Composite N=0
m = 4;
h = (b-a)/m;
sp = a;
y_Comp_N0 = 0;

for i = 1:4 
    y_Comp_N0 = y_Comp_N0 + (sp+h-sp)*f(sp);
    sp = sp+h;
end

disp('Composite n=0');
disp(y_Comp_N0);
disp('Error');
disp(-y_Comp_N0+properValue);

% Composite N=1
m = 4;
h = (b-a)/m;
sp = a;

y_Comp_N1 = 0;

for i = 1:4
y_Comp_N1 = y_Comp_N1 + (h/2)*(f(sp)+f(sp+h));
sp = sp + h;
end

disp('Composite n=1');
disp(y_Comp_N1);
disp('Error');
disp(-y_Comp_N1+properValue);

% Composite N=2
m = 2;
h = (b-a)/m;
sp = a;
y_Comp_N2 = 0;
x0 = sp;
x1 = sp + h/2;
x2 = sp + h;

for i = 1:2
    y_Comp_N2 = y_Comp_N2 + (h/3) * (f(x0) + 4 * f(x1) + f(x2));
    x0 = sp + h;
    x1 = x0 + h/2;
    x2 = x0 + h;
end

disp('Composite n=2');
disp(y_Comp_N2);
disp('Error');
disp(-y_Comp_N2+properValue);

% Composite N=0 M=20
m = 20;
h = (b-a)/m;
sp = a;
y_Comp_N0_M20 = 0;

for i = 1:20 
    y_Comp_N0_M20 = y_Comp_N0_M20 + (sp+h-sp)*f(sp);
    sp = sp+h;
end

disp('Composite n=0');
disp(y_Comp_N0_M20);
disp('Error');
disp(-y_Comp_N0_M20+properValue);

% Composite Chebyshew N=2 M=2
m = 2;
h = (b-a)/m;
b1 = a+h;

x_11 = 0.5 * (a + b1) + 0.5 * (a - b1) * -0.57735;
x_21 = 0.5 * (a + b1) + 0.5 * (a - b1) * 0.57735;
x_12 = 0.5 * (b1 + b) + 0.5 * (b1 - b) * -0.57735;
x_22 = 0.5 * (b1 + b) + 0.5 * (b1 - b) * 0.57735;

y_CompCheb = 0;
y_CompCheb = y_CompCheb + ((b1-a)/2) * (f(x_11)+f(x_21));
y_CompCheb = y_CompCheb + ((b-b1)/2) * (f(x_12)+f(x_22));

disp('Composite Chebyshew n=2');
disp(y_CompCheb);
disp('Error');
disp(-y_CompCheb+properValue);
