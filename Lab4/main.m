close all; clc; clear;

interval_begining = -3;
interval_end = 3; 
step = 0.1;
x0 = interval_begining:step:interval_end;     
h = 0.001;    
n = 10;

f_step = (interval_end-interval_begining)/9999; % +1
x = interval_begining:f_step:interval_end;
yf = (f(x));
yd = zeros(1,length(x));

for i=1:length(x)
    yd(i) = (df(x(i)));
end

result = zeros(1, length(x0));
for i=1:length(x0)
    result(i) = Taylor(x0(i), n, h);
end

error = zeros(1,length(x0));
for i=1:length(x0)
    disp(""+i+": f("+x0(i)+") = (A)"+result(i)+" ~ (R)"+df(x0(i)));
    error(i) = df(x0(i)) - result(i);
end

for i=1:length(error)
    if (abs(error(i)) == max(abs(error))) 
        max_error = error(i);
        val = i;
    end
end
disp("Max Error:");
disp(""+max_error+" at point "+val);

p = figure();
hold on;
plot(x,yd,'-g');
plot(x0,result,'-r');
legend("1st derivative", "Approximation");
saveas(p, 'plot.png');