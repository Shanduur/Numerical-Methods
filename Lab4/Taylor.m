function [result] = Taylor(x0, n, h)
xh_taylor = zeros(1,n+1);
for i=1:n+1
    xh_taylor(i) = x0 + ((i-1)*h);
end

tmp1_t = zeros(1,n);
tmp2_t = zeros(1,n-1);
delta_t = zeros(1,n);

for i=1:n+1
    tmp1_t(i) = f(xh_taylor(i));
end
disp("XH:");
disp(xh_taylor);
disp("Table:");
disp("f(x)");
disp(tmp1_t);

tmp_n = n;

for i=1:tmp_n
    for j=1:tmp_n
        tmp2_t(j) = tmp1_t(j+1)-tmp1_t(j);
    end
    delta_t(i) = tmp2_t(1);
    tmp1_t = tmp2_t;
    tmp2_t = zeros(1,tmp_n-1);
    tmp_n = tmp_n - 1; 
    disp("Forward difference "+i);
    disp(tmp1_t);
end

disp("Choosen:");
disp(delta_t);

result = 0;
for i=1:n
    add = (1/i)*delta_t(i);
    result = result + add;
end

result = (1/h)*result;