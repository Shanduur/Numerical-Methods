clc; 
clear;
close all;

% function 'f' is equal to 0

section = 3;

max_t = 1;
min_t = 0;
max_z = 1;
min_z = 0;

r_step = 0.1;
h_step = 0.1;
a_squared = 0.1*section;


if (((a_squared*h_step)/r_step) > 0.5)
    disp('Convergence criterion not satisfied')
elseif (mod((max_t-min_t),h_step) ~= 0)
    disp('T is not divisible')
elseif (mod((max_z-min_z),r_step) ~= 0)
    disp('Z is not divisible')
else
    z = min_z:r_step:max_z;
    t = min_t:h_step:max_t;
    
    w=zeros(length(t),length(z));
    w(1,1) = 0;
    w(1,length(z)) = 0;

    for i = 2:length(z)-1
        w(1,i) = sin(pi*z(i));
    end
    for i = 1:length(t)-1
        for k = 2:length(z)-1
            w(i+1,k)=w(i,k)+a_squared*h_step*(r_step^(-2))*(w(i,k+1)-2*w(i,k)+w(i,k-1));
        end
    end

    real_val = zeros(length(t),length(z));

    for i = 1:length(t)
        for k = 1:length(z)-1
            real_val(i,k) = exp(-pi^2*a_squared*t(i))*sin(pi*z(k));
        end
    end 

    [Z,T] = meshgrid(z,t); 
    subplot(1,2,1);    
    % Net method
    mesh(Z,T,w);
    title('Net method solution 3D plot');
    xlabel('Z');
    ylabel('Y');
    zlabel('W');
    hold on;
    
    % Exact Val
    subplot(1,2,2);
    mesh(Z,T,real_val);
    title('Exact value 3D plot');
    xlabel('Z');
    ylabel('Y');
    zlabel('W');

    sum=0;
    for i = 1:length(t)
        for k = 1:length(z)
            sum = sum + (real_val(i,k)-w(i,k)).^2;
        end
    end
    MSE = sum/(length(t)*length(z)); 
    
    disp("Net method obtained values:");
    disp(w);
    disp("Real Values:");
    disp(real_val);
    disp("MSE:");
    disp(MSE);
    disp("h_step:");
    disp(h_step);
    disp("r_step:");
    disp(r_step);
end