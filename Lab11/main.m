clear all;
close all;
clc;

nodes = 10;
net = zeros(nodes+1,nodes+1);
r_step = 0.02;
h_step = 0.02;

asq = 0.3;

asq = asq*asq;

disp((asq*h_step)/r_step);
if ((asq*h_step)/r_step>0.5)
    disp('error');
end

init_net = zeros(nodes,nodes);

for i=nodes:-1:1
    net(i,1) = sin(pi*i*h_step); 
end

t = 0;

for j=1:nodes
    t=t+h_step;
    z=0;
    for i=nodes:-1:1
        init_net(i,j)=exp(-pi*pi*asq*t)*sin(pi*z);
        z=z+r_step;
    end
end

disp(net);

for i=1:nodes-1
    for j=1:nodes-1
        if(i-1==0)
                
        else
            net(i,j+1)=net(i,j)+asq*h_step*((r_step)^-2)*(net(i+1,j)-2*net(i,j)+net(i-1,j));
        end
    end
end

disp(net);
disp(init_net);

val = 1;
for i=1:nodes
    for j=1:nodes
        X(i,j)=i;
        Y(i,j)=j;   
        z1(val)=net(i,j);
        z2(val)=init_net(i,j);
        x(val)=X(i,j);
        y(val)=Y(i,j);
        val=val+1;
    end
end

subplot(1,2,1);
surf(X,Y,net,'.r');
hold on;
subplot(1,2,2);
surf(X,Y,init_net,'.b');

