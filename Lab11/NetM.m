clc; clear; close all;
%initial
r = 0.1;
h = 0.005;
a = 0.8;
z = 0:r:1;
t = 0:h:1;
w=zeros(length(t),length(z));
w(1,1) = 0;
w(1,length(z)) = 0;

for i = 2:length(z)-1
    w(1,i) = sin(pi*z(i));
end
for i = 1:length(t)-1
   for k = 2:length(z)-1
       w(i+1,k)=w(i,k)+a*h*(r^(-2))*(w(i,k+1)-2*w(i,k)+w(i,k-1));
       %we don't adding function 'f' because is equal 0
   end
end 
y = zeros(length(t),length(z));
for i = 1:length(t)
   for k = 1:length(z)-1
       y(i,k) = exp(-pi^2*a*t(i))*sin(pi*z(k));
   end
end 
[Z,T] = meshgrid(z,t); 
subplot(2,1,1);
mesh(Z,T,w);
title('net method');
hold on;
subplot(2,1,2);
mesh(Z,T,y);
title('exact value');

sum=0;
for i = 1:length(t)
   for k = 1:length(z)
       sum = sum + (y(i,k)-w(i,k)).^2;
   end
end
MSE = sum/(length(t)*length(z))