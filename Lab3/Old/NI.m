function [v N]=NI(u,x,y)  % Newton's Interpolation
    % vectors x and y contain n+1 points and the corresponding function values
    % vector u contains all discrete samples of the continuous argument of f(x)
    n=length(x);          % number of interpolating points
    k=length(u);          % number of discrete sample points
    v=zeros(1,k);         % Newton interpolation 
    N=ones(n,k);          % all n Newton's polynomials (each of m elements)
    N(1,:)=y(1);          % first Newton's polynomial
    v=v+N(1,:);    
    for i=2:n             % generate remaining Newton's polynomials
        for j=1:i-1
            N(i,:)=N(i,:).*(u-x(j));   
        end
        c=DividedDifference(x,y,i)  % get the ith coefficient c_i
        v=v+c*N(i,:);     % weighted sum of all Newton's polynomials
    end
end

function dd=DividedDifference(x,y,i) % generate f[x_0,...,x_i] in expanded form
    dd=0;
    for k=1:i             % loop for summation
        de=1;
        for l=1:i         % loop for product
            if k~=l   
                de=de*(x(k)-x(l));               
            end
        end
        dd=dd+y(k)/de;    % ith coefficient c_i
    end
end

function dd=DividedDifferenceMatrix(x,y) % generate divided difference matrix
    n=length(x);                         % the coefficients are along diagonal
    dd=zeros(n);                         % matrix of divided differences
    dd(:,1)=y;
    for i=1:n
        fprintf('%6.3f\t',dd(i,1))
        for j=2:i
            dd(i,j)=(dd(i,j-1)-dd(i-1,j-1))/(x(i)-x(i-j+1));
            fprintf('%6.3f\t',dd(i,j));
        end
        fprintf('\n');
    end
end