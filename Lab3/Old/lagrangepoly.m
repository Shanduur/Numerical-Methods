close all; clc; clear;

data = 1;

if data == 1  
    X = [5 6 9 11]; % inputting the values of given x
    Y = [12 13 14 16]; % inputting the values of given y
end

if data == 2
    X = [1 2 3 4 5 6 7 8]; % inputting the values of given x
    Y = [0 1 0 1 0 1 0 1]; % inputting the values of given y
end
    
XX = 0.5 : 0.01 : 8.5; 
    
if size(X,1) > 1;  X = X'; end % checking for parameters 
disp("size(X,1):");
disp(size(X,1));
if size(Y,1) > 1;  Y = Y'; end
disp("size(Y,1):");
disp(size(Y,1));

if size(X,1) > 1 || size(Y,1) > 1 || size(X,2) ~= size(Y,2)
    error('both inputs must be equal-length vectors') % displaying error
end
disp("Y:");
disp(Y);
disp("X:");
disp(X);

N = length(X);
disp("N:");
disp(N);

pvals = zeros(N,N);
% for evaluating  the polynomial weights for each order
for i = 1:N
    % the polynomial with roots may be values of X other than this one
    pp = poly(X( (1:N) ~= i));
    pvals(i,:) = pp ./ polyval(pp, X(i)); % right array division (A./B is the matrix with elements A(i,j)/B(i,j).) 
                        % evaluation of polynomial pp value at every X(i)
end
disp("PP:");
disp(pp);
disp("pvals:");
disp(pvals);

P = Y*pvals;
disp("P:");
disp(P);