function [ ans ] = lag(x,n,X,Y)
    W = zeros(1,n);
    ans = 0;
    
    for i = 1:n
     W(i) = prod(x-X) / (x-X(i));
     for j = 1:n
         if j ~= i
            W(i) = W(i) / (X(i) - X(j));
         end
      end
    end
    disp(W);
    for i = 1:n
        ans = ans + (W(i) * Y(i));
    end
end

