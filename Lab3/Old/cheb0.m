function [ ans ] = cheb(x,degree,points,Y,a,b)
    W = zeros(1,degree+1);
    ans = 0;
    Z = zeros(1,degree+1);
    
    for i = 1:degree+1
        Z = 0.5*((b-a)*x+a+b);
    end
    
    for i = 1:degree+1
     W(i) = prod(Z-points) / (Z-points(i));
     for j = 1:degree+1
         if j ~= i
            W(i) = W(i) / (points(i) - points(j));
         end
      end
    end

    for i = 1:degree+1
        ans = ans + (W(i) * Y(i));
    end
end