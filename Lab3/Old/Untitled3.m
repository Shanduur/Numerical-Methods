close all; clc; clear;

n = 9;
a = 1;
b = 6;

x = zeros(1,n);

%x(1) = (abs(a) + abs(b))/n;
for i = 1:n
    x(i) = i*(abs(b)-abs(a))/n;
end

xf = a:0.1:b;
yf = zeros(1,length(xf));
yl = zeros(1,n);
yc = zeros(1,n);

for i=1:length(xf)
    yf(i) = f(xf(i));
end

plot(xf,yf,'-g');
hold on;

for i=1:n
    yl(i) = lag(x(i),n,xf,yf);
end

plot(x,yl,'.r');
hold on;