function [ out ] = lag(x,n,xf,yf)
    out = zeros(1,n);
    w = ones(1,n);
    
    for i = 1:n
        for j = 1:n
            if j ~= i
                w(i) = w(i)*(x(i)-xf(j));
            end
        end
        for j = 1:n
            if (x(i)-x(j)) ~= 0
                w(i) = w(i) / (x(i) - x(j));
            end
        end
        out(i) =  yf(i)*w(i);
    end    
