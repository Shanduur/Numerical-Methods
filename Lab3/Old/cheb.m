function [ out ] = cheb(x,n,xf,yf,a,b)
    out = zeros(1,n);
    w = ones(1,n);
    
    for i = 1:n
        z(i) = 0.5*((b-a)*x(i)+a+b);
    end
    
    for i = 1:n
        for j = 1:n
            if j ~= i
                w(i) = w(i)*(z(i)-xf(j));
            end
        end
        for j = 1:n
            if (x(i)-x(j)) ~= 0
                w(i) = w(i) / (z(i) - x(j));
            end
        end
        out(i) =  yf(i)*w(i);
    end    
