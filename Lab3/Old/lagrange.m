close all; clc; clear;

interval_s = 1; 
interval_e = 8;
n = 5;

x = 2;
%points_x = interval_s + (interval_e-interval_s)*rand(n+1,1);
points_x = zeros(1,n+1);

points_x(1) = (abs(interval_s) + abs(interval_e))/n;
for i = 2:n
    points_x(i) = points_x(i-1) + (abs(interval_s) + abs(interval_e))/n;
end

points_y = zeros(1,n+1);
for i = 1:n+1
    points_y(i) = f(points_x(i));
end

X = 0:0.1:10;
Y = zeros(length(X),1);
Ylag = zeros(length(X),1);
Ycheb = zeros(length(X),1);

for i = 1:length(X)
    Y(i) = f(X(i));
end

for i = 1:length(X)
   Ylag(i) = lag(X(i),n,points_x,points_y);
end


for i = 1:length(x)
    Ycheb(i) = cheb(X(i),n,points_x,points_y,interval_s, interval_e);
end
    
plot(X,Y,'-r');
hold on;
plot(X,Ylag,'.g','MarkerSize',4);
hold on;
plot(X,Ycheb,'.b','MarkerSize',4);
hold on;