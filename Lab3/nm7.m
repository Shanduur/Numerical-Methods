close all; clc; clear;

%n = 12;     interval_begining = -12.1;     interval_end = 13.1;  n_chunks = 99;
%n = 4;     interval_begining = -1;        interval_end = 1;     n_chunks = 99;
%n = 60;    interval_begining = -100;      interval_end = 100;   n_chunks = 99;
n = 5;    interval_begining = -10;       interval_end = 10;    n_chunks = 99;

chunk = (interval_end-interval_begining)/n_chunks;
disp("Distance between points:");
disp(chunk);

x = interval_begining:chunk:interval_end;

equidistant_points = interval_begining:((interval_end-interval_begining) / (n-1)):interval_end;  % equidistant
parse_Y = zeros(1,n);

for i = 1:n
    parse_Y(i) = f(equidistant_points(i));
end

chebushev_points = zeros(1,n);  % chebyshev
parseY_cheb = zeros(1,n);

for i = 1:n
   chebushev_points(i) = cos(((2*i+1)/(2*n+2))*pi);
end

for i = 1:n
   chebushev_points(i) = ((interval_end-interval_begining)*chebushev_points(i) + interval_begining + interval_end) / 2;
end

for i = 1:n
   parseY_cheb(i) = f(chebushev_points(i));
end

function_Y = zeros(1,n);
lagrange_Y = zeros(1,n);
chebyshev_Y = zeros(1,n);

for i = 1:100
    function_Y(i) = f(x(i));
    lagrange_Y(i) = lag(x(i),equidistant_points,parse_Y,n);
    chebyshev_Y(i) = lag(x(i),chebushev_points,parseY_cheb,n);
end

%disp("x:");
%disp(x);
%disp("Default function Y:");
%disp(function_Y);
%disp("Lagrange approximated Y:");
%disp(lagrange_Y);
%disp("Chebyshev approximated Y:");
%disp(chebyshev_Y);

plot(x,function_Y,'-b');
hold on;
plot(x,lagrange_Y,'-g');
hold on;
plot(x,chebyshev_Y,'-r');
legend('Function','Lagrange','Chebyshev');

errorL = max(function_Y-lagrange_Y);
errorC = max(function_Y-chebyshev_Y);

disp("Max Lagrange Error");
disp(errorL);
disp("Max Chebyshev Error");
disp(errorC);


