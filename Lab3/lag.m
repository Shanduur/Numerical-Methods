function [ out ] = lag(x,X,Y,degree)
   
    W = ones(1,degree);
    out = 0;
    
    for outer_iterator = 1:degree
        
     for inner_iterator = 1:degree
         if inner_iterator ~= outer_iterator
            W(outer_iterator) = W(outer_iterator) * (x-X(inner_iterator));
         end
     end
      
     for inner_iterator = 1:degree
         if inner_iterator ~= outer_iterator
            W(outer_iterator) = W(outer_iterator) / (X(outer_iterator) - X(inner_iterator));
         end
     end
      
    end

    for outer_iterator = 1:degree
        out = out + (W(outer_iterator) * Y(outer_iterator));
    end
end

