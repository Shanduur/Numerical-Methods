int main()
{
    std::vector<double> a; //array of a_0 ... a_n  std::vector<double> b;                  //array of b_0 ... b_n     double Lw[2];

    switch (SYSTEM_CHOICE)
    {
    case 1:
        Lw[0] = -M_PI; //interval for set a             Lw[1] = M_PI;             break;         case 2:             Lw[0] = 0;      //interval for set b             Lw[1] = M_PI;             break;     }

        double x0, error, max_difference, x_max, f_w_difference;
        double W_n = 0;
        int n;
        double acc = 0.001;

        cout << "L2w [" << Lw[0] << ", " << Lw[1] << "]" << endl;
        cout << "Give desired accuracy: ";
        cin >> acc;
        cout << endl;

        n = 0;
        do //quadratic approximation algorithm     {         b.push_back(integration(Lw[0], Lw[1], q_2, n));             //calculating b_k         a.push_back(integration(Lw[0], Lw[1], f_q, n) / b[n]);      //calculating a_k         a[n] = zero_approx(a[n]);

            cout << "b_" << n << "\t= " << b[n] << endl;
        cout << "a_" << n << "\t= " << a[n] << endl;

        error = 0;
        for (int i = 0; i <= n; i++) //calculating p2(f, Wn)         {             error -= pow(a[i], 2) * b[i];         }         error += integration(Lw[0], Lw[1], f_2);         error = zero_approx(error);         n++;     }     while(n <= MAX_ORDER && error >= acc);

            n--;
        cout << endl
             << "Order:\tn= " << n << endl;
        cout << "p(f, W_" << n << ") = " << error << endl
             << endl;

        x0 = Lw[0];
        for (int i = 0; i <= CHECKING_STEPS; i++) //searching for biggest difference  {      x0 = Lw[0] + (i * (Lw[1] - Lw[0]) / CHECKING_STEPS);      W_n = 0;      for(int j = 0; j <=n; j++)                                      //calculating Wn(x) for given x         {             W_n += a[j] * q(x0, j);         }

            f_w_difference = fabs(W_n - f(x0));

        if (i == 0 || f_w_difference > max_difference)
        {
            x_max = x0;
            max_difference = f_w_difference;
        }
    }

    max_difference = zero_approx(max_difference);
    cout << "max |f(x) - W_" << n << "(x)| = " << max_difference << endl
         << endl;

    return 0;