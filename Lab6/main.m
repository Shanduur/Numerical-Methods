clc; clear; close all;

%-----------------------Quadratic approximation----------------------------
% Input: [0,Pi], n
% yi(x) = sqrt(2/Pi)sin(i*x) - orthonormal
% Output: ai = ?, i=0,..,n, Wi(x)=sum_{i=0}^n(ai*yi(x))

l_lim = 0;
u_lim = pi;
n = 10;

a=zeros(1,n);
b=zeros(1,n);

for i=1:n
    b(i) = integral(@(x) (sqrt(2/pi).*sin(i.*x)).^2,l_lim,u_lim);
    a(i) = 1/b(i)*integral(@(x) 5.*sin(5.*x).*sqrt(2/pi).*sin(i.*x),l_lim,u_lim);
end

disp("A:");
disp(a);
disp("B:");
disp(b);

%x=l_lim:0.01:u_lim;
%plot(x,5.*sin(x),'-g');
%hold on;
%x1=l_lim:(u_lim-l_lim)/(n-1):u_lim;
%plot(x1,b,'-r');
%plot(x1,a,'-g');

%-----------------------Point approximation--------------------------------
% Input: [a,b], n, N
% Output: ai = ?, i=0,..,n, Wi(x)=sum_{i=0}^n(ai*x^i)
